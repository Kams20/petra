import {
    FETCH_REPORT,FETCH_UNQ_PROJECTS,FETCH_UNQ_RELEASE,FETCH_UNQ_APPLICATION,SELECTED_PROJECT,RECENT_PROJECT,SELECTED_RELEASE,FETCH_UNQ_TRANSCATION,SELECTED_TRANSCATION   
   } from "../actions/types";
   
   const initialState = {
     allreports: [],
     unqProjects:[],
     unqApplication:[],
     unqRelease:[],
     unqTranscation: [],
     selectedProject: null,
     recentData:{},
     selectedRelease:[],
     selectedTranscation:[]
   };
// eslint-disable-next-line import/no-anonymous-default-export
   export default function (state = initialState, action: any) {
    switch (action.type) {
      case FETCH_REPORT:
        return {
          ...state,
          allreports: action.payload,
        };
        case FETCH_UNQ_PROJECTS:
        return {
          ...state,
          unqProjects: action.payload,
        };
        case FETCH_UNQ_APPLICATION:
        return {
          ...state,
          unqApplication: action.payload,
        };
        case FETCH_UNQ_RELEASE:
        return {
          ...state,
          unqRelease: action.payload,
        };
        case FETCH_UNQ_TRANSCATION:
          return {
            ...state,
            unqTranscation: action.payload,
          };
        case SELECTED_PROJECT:
        return {
          ...state,
          selectedProject: action.payload,
        };
        case SELECTED_RELEASE:
          return {
            ...state,
            selectedRelease: action.payload,
          };
          case SELECTED_TRANSCATION:
            return {
              ...state,
              selectedTranscation: action.payload,
            };
        case RECENT_PROJECT:
          return {
            ...state,
            recentData: action.payload,
          };
      default:
        return state;
    }
  }
  