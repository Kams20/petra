export function maxDateFinding(alldata: any) {
  let allDate: any = [];
  let formattedDate: any = [];
  let recentDatesArray: any = [];

  alldata.forEach((data: any) => {
    allDate.push(data.EXECUTEDON);
  });
  allDate = allDate.filter(
    (value: any, index: any) => allDate.indexOf(value) === index
  );
  allDate.forEach((date: any) => {
    formattedDate.push(new Date(BeforeDateFormatter(date)));
  })
  allDate = formattedDate;
  for (let i = 0; i < 2; i++) {
    let latest = AfterDateFormatter(new Date(Math.max.apply(null, allDate)));
    recentDatesArray.push(latest);
    allDate.forEach((remove: any, index: any) => {
      if (remove.getTime() === new Date(BeforeDateFormatter(latest)).getTime()) {
        allDate.splice(index, 1);
      }
    });
  }
  return recentDatesArray;
}

function BeforeDateFormatter(value: any) {

  let newData = value.split("/");
  value = newData[1] + "/" + newData[0] + "/" + newData[2];
  var DateObj = new Date(value);
  var months = ValidateDate(DateObj.getMonth() + 1);
  var Date1 = ValidateDate(DateObj.getDate());
  var year = DateObj.getFullYear();
  let res = year + "/" + months + "/" + Date1;
  return res;
};

function AfterDateFormatter(value: any) {
  var DateObj = new Date(value);
  var months = ValidateDate(DateObj.getMonth() + 1);
  var Date1 = ValidateDate(DateObj.getDate());
  var year = DateObj.getFullYear();
  return Date1 + "/" + months + "/" + year;
};

const ValidateDate = (data: any) => {
  if (data < 10) {
    data = "0" + data;
  }
  return data;
};

