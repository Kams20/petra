import { config } from "../config/config";

/*By Executed on */
export const RequirementCoverageMainLevel = (
    allreports: any, Type: string, Domain: string, Application: string
  ) => {
    if (allreports !== undefined) {
      let sortingOrder:any = [];
      let toscaReportData:any = [];
      if (Type !== "") {
        toscaReportData = allreports.filter((data: any) => { return (data.Type === Type) });
      }
      if (Domain !== "") {
        toscaReportData = allreports.filter((data: any) => { return (data.Domain === Domain) });
      }
      if (Application !== "") {
        toscaReportData = allreports.filter((data: any) => { return (data.Application === Application) });
      }
      toscaReportData.forEach((sort:any) => {
        sortingOrder.push(sort.Date);
      });
      if (sortingOrder.length !== 0) {
        sortingOrder = sortingOrder.filter(
          (value:any, index:any) => sortingOrder.indexOf(value) === index
        );
      }
      if (toscaReportData !== undefined && toscaReportData.length !== 0) {
        let series = [];
        let ReqCoverage = [];
        for (let i = 0; i < toscaReportData.length; i++) {
          let testExecutionData = toscaReportData[i];
            if (ReqCoverage.length === 0) {
              ReqCoverage.push(testExecutionData.Application);
              let data = [];
              let drillobject = {
                name: "Passed",
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.ReqCoverage,
                // pointWidth: 100,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let ReqCoverageFind = ReqCoverage.filter((col) => {
                return col === testExecutionData.ReqCoverage;
              });
              if (ReqCoverageFind.length === 0) {
                ReqCoverage.push(testExecutionData.ReqCoverage);
                let data = [];
                let drillobject = {
                  name: testExecutionData.ProjectName,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.ReqCoverage,
                  // pointWidth: 100,
                  data: data,
                };
                series.push(seriesObj);
              } else {
                // let index = ReqCoverage.findIndex(
                //   (img) => img === testExecutionData.ReqCoverage
                // );
                let EditSerisObj = series.filter((col) => {
                  return col.name === testExecutionData.ReqCoverage;
                });
                let drillobject = {
                  name: testExecutionData.ProjectName,
                  y: testExecutionData.count,
                };
                EditSerisObj[0].data.push(drillobject);
              }
            }
        }
        let Finalseries:any = [];
        series.forEach((s) => {
          s.data = addSeriesMultilevel(s.data, sortingOrder);
          Finalseries.push(s);
        });
        // Finalseries = SeriesOrderForMainFunction(Finalseries);
        return Finalseries;
      } else {
        return [];
      }
    } else {
      return [];
    }
  };
  
  
  export const addSeriesMultilevel = (data: any, sortingOrder: any) => {
    let FinalData: any = [];
    if (data.length !== 0) {
      data.forEach((d: any) => {
        let sum = 0;
        if (FinalData.length === 0) {
          let FilterElement = data.filter((subdata: any) => {
            return d.name === subdata.name;
          });
          FilterElement.forEach((datasum: any) => {
            sum = sum + datasum.y;
          });
          let finalobj = {
            name: d.name,
            y: sum,
            Domain: d.Domain,
            Type: d.Type,
          };
          FinalData.push(finalobj);
        } else {
          let m = FinalData.filter((matchdata: any) => {
            return d.name === matchdata.name;
          });
          if (m.length === 0) {
            let FilterElement = data.filter((subdata: any) => {
              return d.name === subdata.name;
            });
            FilterElement.forEach((datasum: any) => {
              sum = sum + datasum.y;
            });
            let finalobj = {
              name: d.name,
              y: sum,
              Domain: d.Domain,
              Type: d.Type,
            };
            FinalData.push(finalobj);
          }
        }
      });
    }
    return Datastoring(FinalData, sortingOrder);
  }
  

  /*Sorting Function*/

export function Datastoring(data: any, sortOrder: any) {
    let FinalArray: any = [];
    for (let b = 0; b < sortOrder.length; b++) {
      let TempItem = data.filter((col: any) => {
        return col.name === sortOrder[b];
      });
      if (TempItem.length === 0) {
        let tempObj = {
          name: sortOrder[b],
          y: 0
        }
        FinalArray.splice(b, 0, tempObj);
        data.splice(b, 0, tempObj);
      }
      if (TempItem.length !== 0) {
        FinalArray.push(TempItem[0]);
      }
    }
    return FinalArray;
  }
  