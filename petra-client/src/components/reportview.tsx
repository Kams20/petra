import React, { Component } from "react";
import { Layout, Select,Divider,Breadcrumb } from "antd";
import "antd/dist/antd.css";
import { config } from "../config/config";
import Logo from "../assets/pvlogo_new.png";
import PtReportCharts from "./ptreportcharts";
import AgentReportCharts from "./agentReport";
import { connect } from "react-redux";
import { fetchReport, fetchUniqueProject, fetchUniqueRelease, SelectedProject, SelectedRelease, SelectedTranscation } from "../redux/actions/reportAction";

const { Header, Content } = Layout;
const { Option } = Select;
let firstload = false;
let intitalTranscationset = false;
let PreviousProject = "";

/* Declaring Props variable */
type MyProps = {
  fetchReport: any;
  allReports: any;
  unqRelease: any;
  unqProjects: any;
  fetchUniqueProject: any;
  unqApplication:any;
  fetchUniqueRelease: any;
  SelectedProject: any;
  recentData: any;
  SelectedRelease: any;
  releaseData: any;
  unqTranscation: any;
  SelectedTranscation: any;

};

/* Declaring State variable */
type MyState = {
  collapsed: boolean;
  commercialLine: boolean;
  chartType: any;
  projectValue: any;
  releaseValue: any;
  transactionValue: any;
  allLob: any;
  dropdown: boolean;
  unquieTransaction: any;
  unquieApplication:any;
  unquieRelease:any;

};

class ReportView extends Component<MyProps, MyState> {
  state = {
    collapsed: false,
    allLob: false,
    commercialLine: true,
    chartType: config.release,
    projectValue: this.props.recentData.ProjectName,
    releaseValue: [],
    dropdown: false,
    transactionValue: [],
    unquieTransaction: [],
    unquieApplication:[],
    unquieRelease:[]
  };

  componentWillMount() {
    PreviousProject = this.props.recentData.ProjectName;
  }

  onCollapse = (collapsed: any) => {
    this.setState({ collapsed });
  };

  navigator = (nav: any) => {
    if (nav === config.cLine) {
      this.setState({ commercialLine: true, allLob: false });
    } else if (nav === config.allLob) {
      this.setState({ commercialLine: false, allLob: true });
    } else {
      this.setState({ commercialLine: false, allLob: false });
    }
  };

  ByRelease = () => {
    this.setState({ chartType: config.release, commercialLine: false, dropdown: false });
  };


  ProjectSelect = (params: any) => {
    this.props.SelectedProject(params);
    this.setState({ projectValue: params, releaseValue: [], transactionValue: [] })
    this.props.SelectedRelease([]);
    this.props.SelectedTranscation([]);
    this.props.fetchUniqueRelease(params);
  };

  ReleaseSelect = (params: any) => {
    console.log(this.state.releaseValue,params)
    this.setState({ releaseValue: params, transactionValue: [] });
    this.props.SelectedTranscation([]);
    this.props.SelectedRelease(params);
    this.TranscationValueSet(params);
  }

  ApplicationSelect = (params:any) => {

  }

  TranscationSelect = (params: any) => {
    this.props.SelectedTranscation(params);
    this.setState({ transactionValue: params });
  }

  TranscationValueSet = (data: any) => {
    let FinalTransaction: any = [];
    let FinalApplication: any = [];
    data.forEach((d: any) => {
      let splitArr = d.split('_');
      let filteredTranscation = this.props.unqTranscation.filter((col: any) => {
        return (
          col.Release === splitArr[0] &&
          col.EXECUTEDON === splitArr[1]
        );
      });
      if (filteredTranscation.length !== 0) {
        filteredTranscation.forEach((d: any) => {
          FinalTransaction = FinalTransaction.concat(d.TRANSACTIONNAME)
        });
        filteredTranscation.forEach((d: any) => {
          FinalApplication = FinalApplication.concat(d.Application)
        });
      }
    });
    FinalTransaction = FinalTransaction.filter(
      (value: any, index: any) => FinalTransaction.indexOf(value) === index
    );
    FinalApplication = FinalApplication.filter(
      (value: any, index: any) => FinalApplication.indexOf(value) === index
    );
    FinalTransaction = FinalTransaction.sort();
    FinalApplication = FinalApplication.sort();
    this.setState({ unquieTransaction: FinalTransaction ,unquieApplication:FinalApplication })
  }

  selectedReleaseValidator() {
    if (this.props.releaseData.length === this.state.releaseValue.length) {
      return false;
    } else if (this.props.releaseData.length < this.state.releaseValue.length) {
      return false;
    } else {
      return true;
    }
  }

  ProjectValidationData() {
    if (PreviousProject === this.state.projectValue) {
      return false;
    } else {
      if (this.props.unqTranscation.length !== 0) {
        if (this.props.unqTranscation[0].ProjectName !== PreviousProject) {
          PreviousProject = this.state.projectValue
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  }

  render() {
    if (firstload === false && Object.keys(this.props.recentData).length !== 0) {
      firstload = true;
      this.props.fetchUniqueRelease(this.props.recentData.ProjectName);
      this.props.SelectedProject(this.props.recentData.ProjectName);
      this.props.SelectedRelease([this.props.recentData.Release + "_" + this.props.recentData.EXECUTEDON]);
      this.setState({ projectValue: this.props.recentData.ProjectName, releaseValue: [this.props.recentData.Release + "_" + this.props.recentData.EXECUTEDON] })
    }
    if (this.selectedReleaseValidator()) {
      this.setState({ releaseValue: this.props.releaseData })
      this.TranscationValueSet(this.props.releaseData)
    }
    if (this.props.unqTranscation.length !== 0 && intitalTranscationset === false) {
      intitalTranscationset = true;
      this.TranscationValueSet([this.props.recentData.Release + "_" + this.props.recentData.EXECUTEDON]);
    }
    if (this.props.releaseData.length !== 0 && this.props.unqTranscation.length !== 0 && this.ProjectValidationData()) {
      this.TranscationValueSet(this.props.releaseData);
    }
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Header className="header" 
        style={{ padding: 0, position: 'fixed', zIndex: 1, width: '100%' }}
        >
          <div className="logo">
            <img
              src={Logo}
              alt="PV"
              className="left-sec pv_logo"
            />
          </div>
          <div 
          style={{ float: "right", marginTop: "-40px",padding: '2px',marginLeft: '-85px',marginRight: '-85px'}}
          >
              <Select
                mode="multiple"
                showSearch
                style={{ width: `${(8 * this.state.transactionValue.length) + 500}px`,height:"100px",backgroundColor:"white" }}
                value={this.state.transactionValue}
                onChange={this.TranscationSelect}
                disabled={this.state.dropdown}
              >
                {this.state.unquieTransaction.map((obj: any) => (
                  <Option key={obj} value={obj}>
                    {obj}
                  </Option>
                ))}
              </Select>
              <span className='header-text'>Select Transaction&nbsp;&nbsp;</span>
          </div>
          <div 
          style={{ float: "right", marginTop: "-40px",padding: '2px', }}
          >
            <Select
              mode="multiple"
              showSearch
              style={{ width: `${(8 * this.state.releaseValue.length) + 500}px`, height:"100px",backgroundColor:"white"}}
              value={this.state.releaseValue}
              onChange={this.ReleaseSelect}
              placeholder="Select Release"
              disabled={this.state.dropdown}
            >
              {this.props.unqRelease.map((obj: any) => (
                <Option key={obj} value={obj}>
                  {obj}
                </Option>
              ))}
            </Select>
            <span className='header-text'>Select Release&nbsp;&nbsp;</span>
          </div>
          {/* <div 
          style={{ float: "right", marginTop: "-40px",padding: '2px', }}
          >
            <Select
              mode="multiple"
              showSearch
              style={{ width: `${(8 * this.state.releaseValue.length) + 500}px`, height:"100px",backgroundColor:"white"}}
              value={this.state.releaseValue}
              onChange={this.ApplicationSelect}
              placeholder="Select Application"
              disabled={this.state.dropdown}
            >
              {this.state.unquieApplication.map((obj: any) => (
                <Option key={obj} value={obj}>
                  {obj}
                </Option>
              ))}
            </Select>
            <span className='header-text'>Select Application&nbsp;&nbsp;</span>
          </div> */}
          <div 
          style={{ float: "right", marginLeft: "auto", marginRight: "5px", marginTop: "-44px",padding: '5px', }}
          >
            <Select
              showSearch
              style={{ width: `100px`, height:"100px",}}
              value={this.state.projectValue}
              onChange={this.ProjectSelect}
              placeholder="Select Project"
              disabled={this.state.dropdown}
            >
              {this.props.unqProjects.map((obj: any) => (
                <Option key={obj} value={obj}>
                  {obj}
                </Option>
              ))}
            </Select>
            {/* <span className='header-text'>Project&nbsp;&nbsp;</span> */}
          </div>
        </Header>
        <Layout>
          <Layout className="site-layout">
            <Content style={{ overflow: 'hidden' }}>
            <Breadcrumb
              className="header-brdcrmbs"
            >
              <Breadcrumb.Item
                onClick={(event) => this.navigator(config.cLine)}
                className={this.state.commercialLine ? ("bread-crumbs-selected-color") : ("bread-crumbs-color")}
              >
                Commercial Line
              </Breadcrumb.Item>
              <Breadcrumb.Item
               onClick={(event) => this.navigator(config.pLine)}
                className={!this.state.commercialLine && !this.state.allLob ? ("bread-crumbs-selected-color") : ("bread-crumbs-color")}
              >
                Personal Line
              </Breadcrumb.Item>
            </Breadcrumb>
            <Divider type="vertical" style={{ width: "45%" }}>
              <div className="site-layout-background report-graph" style={{ padding: 80, textAlign: 'center' }}>
                {this.state.chartType === config.release ? (
                  this.state.commercialLine && !this.state.allLob ? (
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Broker</h1>
                      <div className="chart-view" >
                        <PtReportCharts LOB="CL" chartType={config.release} />
                      </div>
                    </div>
                  ) : (!this.state.commercialLine && !this.state.allLob ? (
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Broker</h1>
                      <div className="chart-view">
                        <PtReportCharts LOB="PL" chartType={config.release} />
                      </div>
                    </div>
                  ) : (
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>{this.state.chartType} - ALL LOB</h1>
                      <div className="chart-view">
                        <PtReportCharts LOB={config.allLob} chartType={config.release} />
                      </div>
                    </div>
                  )

                  )
                ) : null}
              </div>
              </Divider>
              <Divider type="vertical" style={{ width: "50%" }}>
              <div className="site-layout-background report-graph" style={{ padding: 80, textAlign: 'center' }}>
                {this.state.chartType === config.release ? (
                  this.state.commercialLine && !this.state.allLob ? (
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Agent</h1>
                      <div className="chart-view" >
                        <AgentReportCharts LOB="CL" chartType={config.release} />
                      </div>
                    </div>
                  ) : (!this.state.commercialLine && !this.state.allLob ? (
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Agent</h1>
                      <div className="chart-view">
                        <AgentReportCharts LOB="PL" chartType={config.release} />
                      </div>
                    </div>
                  ) : (
                    <div
                      className="content-div"
                    // style={{ padding: 24, minHeight: 360 }}
                    >
                      <h1>Agent</h1>
                      <div className="chart-view">
                        <AgentReportCharts LOB={config.allLob} chartType={config.release} />
                      </div>
                    </div>
                  )

                  )
                ) : null}
              </div>
              </Divider>
            </Content>
            {/* <Footer className="footer-style">PETRA REPORT ©2021 Created by Expleo</Footer> */}
          </Layout>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (state: any) => ({
  allReports: state.reportReducer.allreports,
  unqProjects: state.reportReducer.unqProjects,
  unqApplication: state.reportReducer.unqApplication,
  unqRelease: state.reportReducer.unqRelease,
  unqTranscation: state.reportReducer.unqTranscation,
  recentData: state.reportReducer.recentData,
  releaseData: state.reportReducer.selectedRelease,
});

export default connect(mapStateToProps, {
  fetchReport, fetchUniqueProject, fetchUniqueRelease, SelectedProject, SelectedRelease, SelectedTranscation
})(ReportView);
