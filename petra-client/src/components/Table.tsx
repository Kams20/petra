import React,{ useEffect, useState } from "react" ;
import { connect } from "react-redux";
import { Table } from 'antd';
import {TableViewFunction} from '../redux/actions/toscaAction';
let globalLoad = true;

const columns = [
  {
    title: 'Date',
    dataIndex: 'Date',
    key: 'Date',
  },
  {
    title: 'Domain',
    dataIndex: 'Domain',
    key: 'Domain',
  },
    {
        title: 'Application',
        dataIndex: 'Application',
        key: 'Application',
      },
    {
      title: 'Total TestCase',
      dataIndex: 'TotaltestCase',
      key: 'TotaltestCase',
    },
    {
      title: "Passed",
      dataIndex: "Passed",
      key: 'Passed',
      render(text:any, record:any) {
        return {
          props: {
            style: { background: "green",color:'white' }
          },
          children: <div>{text}</div>
        };
      }
    },
    {
      title: 'Failed',
      dataIndex: 'Failed',
      render(text:any, record:any) {
        return {
          props: {
            style: { background: "red",color:'white' }
          },
          children: <div>{text}</div>
        };
      },
      key: 'Failed',
    },
  ];

const ToscaReportTable = (props:any,state:any) => {
  // useEffect(() => {
  //   //Runs only on the first render
  //   if(globalLoad){
  //     props.TableViewFunction(props.data);
  //     globalLoad = false;
  //   }    
  // },[props,state]);
    
    return(
       <>
       {props.data ? (<Table scroll={{ y: 300 }} dataSource={props.data} columns={columns} pagination={false}></Table>):null} 
       </>
    )
}

const mapStateToProps = (state: any) => ({
  tableData: state.ToscaReducer.tableData,
});

export default connect(mapStateToProps, {
  TableViewFunction
})(ToscaReportTable);