const sql = require("./db.js");

const Report = function (report) {
  this.RUN_TYPE = report.RUN_TYPE;
  this.CHANNEL = report.CHANNEL;
  this.APPLICATION = report.APPLICATION;
  this.Environment = report.Environment;
  this.LOB = report.LOB;
  this.TRANSACTIONNAME = report.TRANSACTIONNAME;
  this.Release = report.Release;
  this.EXECUTEDON = report.EXECUTEDON;
  this.SLA = report.SLA;
  this.ResponseTimeInsecs = report.ResponseTimeInsecs;
};

Report.getAll = (result) => {
  // var qry = "SELECT * FROM performance_testing";
  var qry = "SELECT RUN_TYPE,CHANNEL,APPLICATION,Environment,LOB,TRANSACTIONNAME,`Release`,EXECUTEDON,SLA,CAST(ResponseTimeInsecs AS FLOAT) AS ResponseTimeInsecs,ProjectName,releseNameforBaseline,releseValueforBaseline,Scenario FROM performance_testing";

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};

Report.getByRelease = (project,result) => {
  var qry = `SELECT * FROM performance_testing where ProjectName = '${project.toString()}'`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};

module.exports = Report;
