const sql = require("./db.js");

const Report = function (report) {
  this.Date = report.Date;
  this.Type = report.Type;
  this.Domain = report.Domain;
  this.Application = report.Application;
  this.TotaltestCase = report.TotaltestCase;
  this.Passed = report.Passed;
  this.Failed = report.Failed;
  this.Remarks = report.Remarks;
  this.NooftimesExecuted = report.NooftimesExecuted;
};

Report.getAll = (result) => {
  var qry = "SELECT * FROM toscareport";
//   var qry = "SELECT RUN_TYPE,CHANNEL,APPLICATION,Environment,LOB,TRANSACTIONNAME,`Release`,EXECUTEDON,SLA,CAST(ResponseTimeInsecs AS FLOAT) AS ResponseTimeInsecs,ProjectName,releseNameforBaseline,releseValueforBaseline,Scenario FROM performance_testing";

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};


module.exports = Report;
