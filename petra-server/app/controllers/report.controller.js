const Report = require("../models/report.model");

const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

/* Retrieve all Components from the database. */
exports.findAll = (req, res) => {
  Report.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Reports",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Reports");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Reports List Retrieved",
        userMessage: "Reports List Retrieved Successfully !!!.",
        recentProject:maxDateFinding(data),
        data,       
      });
  });
};


/* Retrieve all Projects from the database. */
exports.projectFilter = (req, res) => {
  Report.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Reports",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Reports");
    else{
      res.send({
        status: "SUCCESS",
        data: projectFormatter(data)
      });
    }
  });
};


/*Formatting Project Data*/

function projectFormatter(data){
  let projectData = [];
  if(data.length !== 0){
    for(let i = 0; i<data.length; i++){
      let project = data[i];
        projectData.push(project.ProjectName)
    }
    projectData = projectData.filter(
      (value, index) => projectData.indexOf(value) === index
    );
  }
  return projectData;
}

/*Formatting Application Data*/

function ApplicationFormatter(data){
  let Application = [];
  if(data.length !== 0){
    for(let i = 0; i<data.length; i++){
      let project = data[i];
      Application.push({
        Release: project.Release,
        EXECUTEDON:project.EXECUTEDON,
        TRANSACTIONNAME: project.TRANSACTIONNAME,
        ProjectName: project.ProjectName,
        Application: project.APPLICATION
      })
    }
  }
  return Application.sort();
}

/* Retrieve Release based on the project */
exports.ReleaseFilter = (req, res) => {
  paramSpliter(req.params.projectName)
  Report.getByRelease(req.params.projectName,(err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Reports",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Reports");
    else{
      res.send({
        status: "SUCCESS",
        applicationData: ApplicationFormatter(data),
        TranscationData: TranscationFormatter(data),
        data: releaseFormatter(data),
      });
    }
  });
};


/*Formatting Project Data*/

function releaseFormatter(data){
  let releaseData = [];
  if(data.length !== 0){
    for(let i = 0; i<data.length; i++){
      let project = data[i];
      releaseData.push(
        project.Release+"_"+project.EXECUTEDON,
      //   {
      //   Release:project.Release+"_"+project.EXECUTEDON,
      //   Application: project.APPLICATION
      // }
      )
    }
    releaseData = releaseData.filter(
      (value, index) => releaseData.indexOf(value) === index
    );
  }
  return releaseData.sort();
}

/*Formatting Project Data*/

function TranscationFormatter(data){
  let TranscationName = [];
  if(data.length !== 0){
    for(let i = 0; i<data.length; i++){
      let project = data[i];
      TranscationName.push({
        Release: project.Release,
        EXECUTEDON:project.EXECUTEDON,
        TRANSACTIONNAME: project.TRANSACTIONNAME,
        ProjectName: project.ProjectName,
        Application: project.APPLICATION
      })
    }
  }
  return TranscationName.sort();
}

/*Params spliter*/

function paramSpliter(data){
  if(data.length !== 0){
    let myArr = data.split(",");
    let newArr = [];
    for(let i=0;i<myArr.length;i++){
      newArr.push(`'${myArr[i]}'`)
    }
    return newArr;
  }else{
    return data;
  }
}

/*Finding Recent Project Data*/
 
 function maxDateFinding(alldata){
  let allDate = new Array();
  let formattedDate = new Array();
  alldata.forEach((data) => {
      allDate.push(data.EXECUTEDON);
  });
  allDate = allDate.filter(
      (value, index) => allDate.indexOf(value) === index
    );
  allDate.forEach((date) => {
      formattedDate.push(new Date(BeforeDateFormatter(date)));
  })
  allDate = formattedDate;  
  let latest = AfterDateFormatter(new Date(Math.max.apply(null, allDate)));
  let RecentProject = alldata.filter(col => {
    return (
      col.EXECUTEDON === latest
    );
  });
  return RecentProject[0];
}

function BeforeDateFormatter (value){
  
  let newData = value.split("/");
  value = newData[1]+"/"+newData[0]+"/"+newData[2];
  var DateObj = new Date(value);
  var months = ValidateDate(DateObj.getMonth() + 1);
  var Date1 = ValidateDate(DateObj.getDate());
  var year = DateObj.getFullYear();
  let res = year + "/" + months + "/" + Date1;
  return res;
};

function AfterDateFormatter (value){
  var DateObj = new Date(value);
  var months = ValidateDate(DateObj.getMonth() + 1);
  var Date1 = ValidateDate(DateObj.getDate());
  var year = DateObj.getFullYear();
  return Date1 + "/" + months + "/" + year;
};

const ValidateDate = (data) => {
  if (data < 10) {
    data = "0" + data;
  }
  return data;
};
