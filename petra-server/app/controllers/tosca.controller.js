const Report = require("../models/tosca.model");

const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

/* Retrieve all Components from the database. */
exports.findAll = (req, res) => {
  Report.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Reports",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Reports");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Reports List Retrieved",
        userMessage: "Reports List Retrieved Successfully !!!.",
        data,       
      });
  });
};
